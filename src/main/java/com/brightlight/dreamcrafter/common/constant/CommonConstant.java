package com.brightlight.dreamcrafter.common.constant;

/**
 * @description: 公共常量类
 * @author: LiMG
 * @create: 2024-04-23 14:39:32
 **/
public class CommonConstant {

    /**
     * 链接文件公共目录名称
     */
    public static final String LINK_FILE_PATH = "link-file";

    /**
     * 空字符串
     */
    public static final String EMPTY = "";
}
