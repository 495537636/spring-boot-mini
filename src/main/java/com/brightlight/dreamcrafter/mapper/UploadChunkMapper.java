package com.brightlight.dreamcrafter.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.brightlight.dreamcrafter.model.entity.UploadFileEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @description: 分片上传Mapper
 * @author: LiMG
 * @create: 2024-04-22 14:34:40
 **/
@Mapper
public interface UploadChunkMapper extends BaseMapper<UploadFileEntity> {

    /**
     * 获取文件是否存在
     * @param md5
     * @return
     */
    Integer getFileExist(@Param("md5") String md5);

    /**
     * 获取上传分片的索引
     * @param md5   文件md5
     * @param md5   存储桶名称
     * @return
     */
    Integer getUploadChunkIndex(@Param("md5") String md5, @Param("bucketName") String bucketName);

}
