package com.brightlight.dreamcrafter.common.exception;

import com.brightlight.dreamcrafter.common.wrap.WrapMapper;
import com.brightlight.dreamcrafter.common.wrap.Wrapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import static com.brightlight.dreamcrafter.common.enums.BaseEnum.ERROR;

/**
 * @description: 全局异常监听类
 * @author: LiMG
 * @create: 2024-04-01 14:40:08
 **/
@Slf4j
@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public Wrapper globalExceptionHandler(Exception e) {
        log.error("发生异常！异常信息：{}", e.getMessage(), e);
        return WrapMapper.wrap(ERROR.getCode(), ERROR.getMessage());
    }

}
