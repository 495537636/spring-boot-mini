package com.brightlight.dreamcrafter.model.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import jakarta.persistence.*;
import lombok.Data;

/**
 * @description: 用户信息实体类
 * @author: LiMG
 * @create: 2024-02-04 23:16:25
 **/
@Data
@Entity
@Table(name = "user_info")
@TableName("user_info")
public class UserInfoEntity {

    @Id
    @Column(name = "id", columnDefinition = "bigint COMMENT '主键id'")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @TableId
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    @Column(name = "username", columnDefinition = "varchar(255) COMMENT '用户名称'")
    private String username;

    @Column(name = "password", columnDefinition = "varchar(255) COMMENT '密码'")
    private String password;

}
