package com.brightlight.dreamcrafter.controller;

import com.brightlight.dreamcrafter.common.util.MinioUtil;
import com.brightlight.dreamcrafter.common.wrap.WrapMapper;
import com.brightlight.dreamcrafter.common.wrap.Wrapper;
import com.brightlight.dreamcrafter.model.dto.UploadFileDTO;
import com.brightlight.dreamcrafter.service.IFileUploadService;
import io.minio.messages.Bucket;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping(value = "/file")
@Tag(name = "文件操作")
public class FileController {

    @Autowired
    private MinioUtil minioUtil;

    @Resource
    private IFileUploadService fileUploadService;

    /**
     * 查看存储bucket是否存在
     *
     * @param bucketName
     * @return
     */
    @GetMapping("/bucketExists")
    @Operation(summary = "查看存储bucket是否存在", description = "查看存储bucket是否存在")
    public Wrapper bucketExists(@RequestParam("bucketName") String bucketName) {
        return WrapMapper.success(minioUtil.bucketExists(bucketName));
    }

    /**
     * 创建存储bucket
     *
     * @param bucketName
     * @return
     */
    @GetMapping("/makeBucket")
    public Wrapper makeBucket(String bucketName) {
        return WrapMapper.success(minioUtil.makeBucket(bucketName));
    }

    /**
     * 删除存储bucket
     *
     * @param bucketName
     * @return
     */
    @GetMapping("/removeBucket")
    public Wrapper removeBucket(String bucketName) {
        return WrapMapper.success(minioUtil.removeBucket(bucketName));
    }

    /**
     * 获取全部bucket
     *
     * @return
     */
    @GetMapping("/getAllBuckets")
    public Wrapper getAllBuckets() {
        List<Bucket> allBuckets = minioUtil.getAllBuckets();
        return WrapMapper.success(allBuckets);
    }

    @GetMapping("/checkFileMd5/{md5}")
    public Wrapper checkFileMd5(@PathVariable String md5) {
        return WrapMapper.success(fileUploadService.checkFileMd5(md5));
    }

    /**
     * 单文件上传
     * @param uploadFileDTO
     * @return
     * @throws Exception
     */
    @PostMapping(value = "/uploadBySingle")
    public Wrapper uploadBySingle(@ModelAttribute UploadFileDTO uploadFileDTO) throws Exception {
            return WrapMapper.success(fileUploadService.uploadBySingle(uploadFileDTO));
    }

    /**
     * 文件秒传
     * @param uploadFileDTO
     * @return
     * @throws Exception
     */
    @PostMapping(value = "/uploadByInstant")
    public Wrapper uploadByInstant(@ModelAttribute UploadFileDTO uploadFileDTO) throws Exception {
        return WrapMapper.success(fileUploadService.uploadByInstant(uploadFileDTO));
    }

    @GetMapping(value = "/getUploadChunkIndex/{md5}/{bucketName}")
    public Wrapper getUploadChunkIndex(@PathVariable String md5, @PathVariable String bucketName) throws Exception {
        return WrapMapper.success(fileUploadService.getUploadChunkIndex(md5, bucketName));
    }

    /**
     * 分片文件上传
     * @param uploadFileDTO
     * @return
     * @throws Exception
     */
    @PostMapping(value = "/uploadByChunk")
    public Wrapper uploadByChunk(@ModelAttribute UploadFileDTO uploadFileDTO) throws Exception {
        return WrapMapper.success(fileUploadService.uploadByChunk(uploadFileDTO));
    }

    /**
     * 获取文件预览地址
     * @param id
     * @return
     */
    @GetMapping("/getFileViewUrl/{id}")
    public Wrapper preview(@PathVariable String id) {
        return WrapMapper.success(fileUploadService.getFileViewUrl(id));
    }

    /**
     * 图片/视频预览
     *
     * @param bucketName    桶名称
     * @param objectName    对象名称
     * @return
     */
    @GetMapping("/preview")
    public Wrapper preview(@RequestParam("bucketName") String bucketName,@RequestParam("objectName") String objectName) {
        return WrapMapper.success(minioUtil.preview(bucketName, objectName));
    }

    /**
     * 文件下载
     *
     * @param fileName
     * @param res
     * @return
     */
    @GetMapping("/download")
    public Wrapper download(@RequestParam("fileName") String fileName, HttpServletResponse res) {
        minioUtil.download(fileName, res);
        return WrapMapper.success();
    }

    /**
     * 根据ID删除文件
     *
     * @param id
     * @return
     */
    @GetMapping("/deleteFile")
    public Wrapper deleteFile(@RequestParam("id") Long id) {
        return WrapMapper.success(fileUploadService.deleteFileById(id));
    }

}