package com.brightlight.dreamcrafter.model.dto;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.io.Serial;
import java.io.Serializable;

/**
 * @description: 文件上传DTO
 * @author: LiMG
 * @create: 2024-04-16 09:41:59
 **/
@Data
public class UploadFileDTO implements Serializable {
    @Serial
    private static final long serialVersionUID = -7043617490321441436L;

    // 文件桶名称
    private String bucketName;

    // 分片文件
    private MultipartFile file;

    // 当前分片索引
    private int chunk;

    // 总分片数据
    private int chunks;

    // 文件名称
    private String fileName;

    // 文件的MD5
    private String md5;

}
