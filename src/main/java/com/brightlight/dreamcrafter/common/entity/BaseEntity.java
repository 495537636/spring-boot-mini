package com.brightlight.dreamcrafter.common.entity;

import com.brightlight.dreamcrafter.common.util.TimeUtil;
import jakarta.persistence.Column;
import jakarta.persistence.MappedSuperclass;
import lombok.Data;

/**
 * @description: 基础实体类
 * @author: LiMG
 * @create: 2024-04-22 14:13:48
 **/
@Data
@MappedSuperclass
public class BaseEntity {

    /**
     * 创建时间
     */
    @Column(name = "create_time", columnDefinition = "varchar(255) COMMENT '创建时间'")
    private String createTime;

    /**
     * 更新时间
     */
    @Column(name = "update_time", columnDefinition = "varchar(255) COMMENT '更新时间'")
    private String updateTime;

    public void setTime() {
        this.setCreateTime(TimeUtil.time());
        this.setUpdateTime(TimeUtil.time());
    }

    public void setUpdateTime() {
        this.setUpdateTime(TimeUtil.time());
    }

}
