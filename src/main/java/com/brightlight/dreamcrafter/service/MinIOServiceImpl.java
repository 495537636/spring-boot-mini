package com.brightlight.dreamcrafter.service;

import cn.hutool.core.util.StrUtil;
import com.brightlight.dreamcrafter.common.util.IDUtil;
import com.brightlight.dreamcrafter.common.util.MinioUtil;
import com.brightlight.dreamcrafter.model.dto.MinIOCredentialDTO;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;
import java.util.Map;

/**
 * @description: MinIO接口实现类
 * @author: LiMG
 * @create: 2024-04-01 14:11:37
 **/
@Service
public class MinIOServiceImpl implements IMinIOService {

    @Resource
    private MinioUtil minioUtil;

    @Override
    public Map<String, String> getUploadCredential(MinIOCredentialDTO minIOCredentialDTO) throws Exception {
        // 如果前端不指定桶，那么给一个默认的
        if (StrUtil.isEmpty(minIOCredentialDTO.getBucketName())) {
            minIOCredentialDTO.setBucketName("test-bucket");
        }

        // 前端不指定文件名称，就给一个UUID
        if (StrUtil.isEmpty(minIOCredentialDTO.getFileName())) {
            minIOCredentialDTO.setFileName(IDUtil.id());
        }

        // 设置凭证过期时间
        ZonedDateTime expireTime = ZonedDateTime.now().plusMinutes(10);

        return minioUtil.signPostPolicy(minIOCredentialDTO.getBucketName(), minIOCredentialDTO.getFileName(), expireTime);
    }

}
