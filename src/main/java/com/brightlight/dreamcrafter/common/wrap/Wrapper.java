package com.brightlight.dreamcrafter.common.wrap;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;

/**
 * <p>Description 返回对象封装类 </p>
 * <p>Company: http://www.uinnova.cn/ </p>
 * @author LiMG
 * @Date 2018/3/28 11:02
 */
@Schema(title = "返回对象")
public class Wrapper<T> implements Serializable {

    private static final long serialVersionUID = -5286986781881970377L;

    @Schema(title = "返回状态码")
    private Integer code;
    @Schema(title = "返回消息")
    private String message;
    @Schema(title = "返回对象")
    private T result;

    public Wrapper() {
        this(200, "操作成功");
    }

    public Wrapper(Integer code, String message) {
        code(code).message(message);
    }

    public Wrapper(Integer code, String message, T result) {
        code(code).message(message).result(result);
    }

    public Integer getCode() {
        return this.code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getResult() {
        return this.result;
    }

    public void setResult(T result) {
        this.result = result;
    }

    public Wrapper<T> code(Integer code) {
        setCode(code);
        return this;
    }

    public Wrapper<T> message(String message) {
        setMessage(message);
        return this;
    }

    public Wrapper<T> result(T result) {
        setResult(result);
        return this;
    }

    @JsonIgnore
    public boolean isSuccess() {
        return 200 == this.code;
    }

    @JsonIgnore
    public boolean isFail() {
        return 200 != this.code;
    }

    @Override
    public String toString() {
        return "Wrapper{code=" + this.code + ", message='" + this.message + '\'' + ", result=" + this.result + '}';
    }

}
