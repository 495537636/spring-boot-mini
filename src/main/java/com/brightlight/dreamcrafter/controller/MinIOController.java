package com.brightlight.dreamcrafter.controller;

import com.brightlight.dreamcrafter.common.annotation.ApiDecrypt;
import com.brightlight.dreamcrafter.common.annotation.ApiEncrypt;
import com.brightlight.dreamcrafter.common.wrap.WrapMapper;
import com.brightlight.dreamcrafter.common.wrap.Wrapper;
import com.brightlight.dreamcrafter.model.dto.MinIOCredentialDTO;
import com.brightlight.dreamcrafter.service.IMinIOService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @description: MinIO接口类
 * @author: LiMG
 * @create: 2024-04-01 13:50:04
 **/
@Slf4j
@ApiEncrypt
@ApiDecrypt
@RestController
@RequestMapping(value = "/minio")
@Tag(name = "MinIO文件操作接口")
public class MinIOController {

    @Resource
    private IMinIOService minIOService;

    @PostMapping("/getUploadCredential")
    @Operation(summary = "获取上传凭证", description = "获取上传凭证")
    public Wrapper getUploadCredential(@RequestBody MinIOCredentialDTO minIOCredentialDTO) throws Exception {
        return WrapMapper.success(minIOService.getUploadCredential(minIOCredentialDTO));
    }

}
