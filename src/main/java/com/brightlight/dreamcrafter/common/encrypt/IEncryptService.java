package com.brightlight.dreamcrafter.common.encrypt;

/**
 * @description: 加密接口
 * @author: LiMG
 * @create: 2024-05-20 16:50:11
 **/
public interface IEncryptService {

    /**
     * 加密
     *
     * @param data
     * @return
     */
    String encrypt(String data);

    /**
     * 解密
     * @param data
     * @return
     */
    String decrypt(String data);

}
