package com.brightlight.dreamcrafter.common.config;

import io.swagger.v3.oas.models.ExternalDocumentation;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @description: swagger配置类
 * @author: LiMG
 * @create: 2024-03-17 22:58:28
 **/
@Configuration
public class SwaggerConfig {
    @Bean
    public OpenAPI restfulOpenAPI() {
        return new OpenAPI()
                .info(new Info().title("赤手筑梦接口文档")
                        .description("赤手筑梦Restful API接口文档")
                        .version("V1.0.0")
                        .license(new License().name("官方网站").url("http://springdoc.org")))
                .externalDocs(new ExternalDocumentation());
    }
}
