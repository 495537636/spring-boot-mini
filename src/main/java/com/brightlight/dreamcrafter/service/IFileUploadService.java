package com.brightlight.dreamcrafter.service;

import com.brightlight.dreamcrafter.model.dto.UploadFileDTO;

import java.util.Map;

/**
 * @description: 文件上传接口
 * @author: LiMG
 * @create: 2024-04-16 09:44:52
 **/
public interface IFileUploadService {

    /**
     * 校验文件的MD5值，判断文件是否存在
     * @param md5
     * @return
     */
    Boolean checkFileMd5(String md5);

    /**
     * 获取上传分片的索引
     * @param md5
     * @param bucketName
     * @return
     * @throws Exception
     */
    Integer getUploadChunkIndex(String md5, String bucketName) throws Exception;

    /**
     * 分片文件上传
     * @param uploadChunk
     * @return
     */
    Map<String, Object> uploadByChunk(UploadFileDTO uploadChunk) throws Exception;

    /**
     * 单文件上传（不分片）
     * @param uploadFileDTO
     * @return
     * @throws Exception
     */
    Map<String, Object> uploadBySingle(UploadFileDTO uploadFileDTO) throws Exception;

    /**
     * 文件秒传
     * @param uploadFileDTO
     * @return
     * @throws Exception
     */
    Map<String, Object> uploadByInstant(UploadFileDTO uploadFileDTO) throws Exception;

    /**
     * 获取文件预览地址
     * @param id
     * @return
     */
    String getFileViewUrl(String id);

    /**
     * 根据ID删除文件
     * @param id
     * @return
     */
    Boolean deleteFileById(Long id);

}
