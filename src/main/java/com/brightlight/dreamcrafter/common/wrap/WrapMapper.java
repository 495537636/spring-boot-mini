package com.brightlight.dreamcrafter.common.wrap;


import com.brightlight.dreamcrafter.common.enums.BaseEnum;

/**
 * <p>Description  </p>
 * <p>Company: http://www.uinnova.cn/ </p>
 *
 * @author LiMG
 * @Date 2018/3/28 11:22
 */
public class WrapMapper {

    public static <E> Wrapper<E> wrap(IResCodeAndMessage resCodeAndMessage) {
        return new Wrapper(resCodeAndMessage.resCode(), resCodeAndMessage.resMessage());
    }

    public static <E> Wrapper<E> wrap(Integer code, String message, E o) {
        return new Wrapper(code, message, o);
    }

    public static <E> Wrapper<E> wrap(Integer code, String message) {
        return new Wrapper(code, message);
    }

    public static <E> Wrapper<E> wrap(Integer code) {
        return wrap(code, null);
    }

    public static <E> Wrapper<E> wrap(Exception e) {
        return new Wrapper(500, e.getMessage());
    }

    public static <E> E unWrap(Wrapper<E> wrapper) {
        return wrapper.getResult();
    }

    /**
     * 系统异常
     *
     * @param <E>
     * @return
     */
    public static <E> Wrapper<E> error() {
        return wrap(BaseEnum.ERROR.getCode(), BaseEnum.ERROR.getMessage());
    }

    public static <E> Wrapper<E> error(String msg) {
        return wrap(BaseEnum.ERROR.getCode(), msg);
    }

    /**
     * 操作成功
     *
     * @param e
     * @param <E>
     * @return
     */
    public static <E> Wrapper<E> success(E e) {
        return wrap(BaseEnum.SUCCESS.getCode(), BaseEnum.SUCCESS.getMessage(), e);
    }

    public static <E> Wrapper<E> success() {
        return wrap(BaseEnum.SUCCESS.getCode(), BaseEnum.SUCCESS.getMessage());
    }

    public static <E> Wrapper<E> ok() {
        return new Wrapper();
    }

    /**
     * 业务异常
     *
     * @param e   异常对象
     * @param <E> 泛型对象
     * @return
     */
    public static <E> Wrapper<E> businessError(Exception e) {
        return wrap(BaseEnum.BUSINESS_ERROR.getCode(), e.getMessage());
    }

    public static <E> Wrapper<E> businessError(String message) {
        return wrap(BaseEnum.BUSINESS_ERROR.getCode(), message);
    }
}
