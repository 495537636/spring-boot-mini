package com.brightlight.dreamcrafter.common.encrypt;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

/**
 * @description: AES数据加解密接口
 * @author: LiMG
 * @create: 2024-05-21 09:19:33
 **/
@Slf4j
@Service
@ConditionalOnProperty(name = "dreamcrafter.encrypt.type", havingValue = "aes")
public class AesEncryptServiceImpl implements IEncryptService {

    /**
     * 数据加密
     * @param data
     * @return
     */
    @Override
    public String encrypt(String data) {
        return data;
    }

    /**
     * 数据解密
     * @param data
     * @return
     */
    @Override
    public String decrypt(String data) {
        return data;
    }
}
