package com.brightlight.dreamcrafter.common.dto;

import lombok.Data;

/**
 * @description: 接口参数加密DTO
 * @author: LiMG
 * @create: 2024-05-25 23:58:14
 **/
@Data
public class ApiEncryptDTO {

    /**
     * 加密数据
     */
    private String encryptData;

}
