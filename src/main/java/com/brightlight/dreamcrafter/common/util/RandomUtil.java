package com.brightlight.dreamcrafter.common.util;

import java.util.Random;

/**
 * @description: 随机字符串工具
 * @author: LiMG
 * @create: 2024-03-17 22:48:34
 **/
public class RandomUtil {

    /**
     * 随机数字典
     * @author pbstudio
     */
    public enum Dict {
        lower_letter("abcdefghijklmnopqrstuvwxyz"), upper_letter(
                "ABCDEFGHIJKLMNOPQRSTUVWXYZ"), number("0123456789");
        private String value;

        private Dict(String str) {
            this.value = str;
        }

        public String getValue() {
            return this.value;
        }
    }

    /**
     * 随机字符串来源
     */
    private String dict;

    /**
     * 默认随机数字
     */
    public RandomUtil() {
        dict = Dict.number.value;
    }

    /**
     * 设置随机字典
     *
     * @param dics
     * @return
     */
    public RandomUtil setDice(Dict... dics) {
        dict = "";
        for (Dict dic : dics) {
            dict += dic.getValue();
        }
        return this;
    }

    /**
     * 自定义随机字典
     *
     * @param source
     * @return
     */
    public RandomUtil setDice(String source) {
        this.dict = source;
        return this;
    }

    /**
     * 生产随机字符串
     * @param length 要生成字符串的长度
     * @return
     */
    public String generate(int length) {
        StringBuffer sb = new StringBuffer();
        Random random = new Random();
        for (int i = 0; i < length; i++) {
            sb.append(this.dict.charAt(random.nextInt(this.dict.length())));
        }
        return sb.toString();
    }

    public static void main(String[] args) {
        System.out.println(new RandomUtil().generate(50));
    }

}
