package com.brightlight.dreamcrafter.common.advice;

import com.alibaba.fastjson.JSON;
import com.brightlight.dreamcrafter.common.annotation.ApiEncrypt;
import com.brightlight.dreamcrafter.common.encrypt.IEncryptService;
import com.brightlight.dreamcrafter.common.wrap.Wrapper;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

/**
 * @description: 接口返回参数加密控制器
 * @author: LiMG
 * @create: 2024-05-20 14:41:45
 **/
@Slf4j
@ControllerAdvice
public class ResponseBodyEncryptAdvice implements ResponseBodyAdvice<Wrapper> {

    @Resource
    private IEncryptService encryptService;

    @Override
    public boolean supports(MethodParameter returnType, Class<? extends HttpMessageConverter<?>> converterType) {
        // 判断方法或类上使用了ApiEncrypt注解
        return returnType.hasMethodAnnotation(ApiEncrypt.class) || returnType.getContainingClass().isAnnotationPresent(ApiEncrypt.class);
    }

    @Override
    public Wrapper beforeBodyWrite(Wrapper body, MethodParameter returnType, MediaType selectedContentType, Class<? extends HttpMessageConverter<?>> selectedConverterType, ServerHttpRequest request, ServerHttpResponse response) {
        if (body.getResult() == null) {
            return body;
        }
        System.out.println(body.getResult());
        String result = encryptService.encrypt(JSON.toJSONString(body.getResult()));
        body.setResult(result);
        return body;
    }

}
