package com.brightlight.dreamcrafter.service;

import com.brightlight.dreamcrafter.model.dto.MinIOCredentialDTO;
import io.minio.errors.*;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Map;

/**
 * @description: MinIO接口
 * @author: LiMG
 * @create: 2024-04-01 14:09:59
 **/
public interface IMinIOService {

    Map<String, String> getUploadCredential(MinIOCredentialDTO minIOCredentialDTO) throws Exception;

}
