package com.brightlight.dreamcrafter.model.dto;

import lombok.Data;

/**
 * @description: MinIO上传凭证
 * @author: LiMG
 * @create: 2024-04-01 13:53:33
 **/
@Data
public class MinIOCredentialDTO {

    /**
     * 存储桶名称
     */
    private String bucketName;

    /**
     * 文件名
     */
    private String fileName;

}
