package com.brightlight.dreamcrafter.common.util;

import org.apache.tomcat.util.buf.StringUtils;

import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * @description: ID生成工具
 * @author: LiMG
 * @create: 2024-03-17 22:49:00
 **/
public class IDUtil {

    /**
     * 获取文件名称(9位字符串)
     *
     * @return
     */
    private static String random() {
        return new RandomUtil().setDice("team").generate(1)+new RandomUtil().generate(10);
    }

    public static String id() {
//        return System.currentTimeMillis() + random();
        return System.currentTimeMillis() + new RandomUtil().generate(6);
    }

    public static String get() {
        return System.currentTimeMillis() + new RandomUtil().generate(6);
    }

    /**
     * 把id反转为日期+随机数
     *
     * @param id
     * @return
     */
    public static String data(String id) {
        String time = id.substring(0, 13);
        try{
            SimpleDateFormat sdf = new SimpleDateFormat("", Locale.CHINESE);
            sdf.applyPattern("yyyy-MM-dd HH:mm:ss:SSS");
            return sdf.format(Long.valueOf(time));
        }catch(Exception e){
            return time;
        }
    }

    public static String getPoiObjectId(int n){
        char[] data={'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
        StringBuilder sb=new StringBuilder();
        for (int i = 0; i < n; i++) {
            int id = (int) Math.ceil(Math.random() * 35);
            sb.append(data[id]);
        }
        return  sb.toString();
    }

    public static String id16() {
        String[] chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".split("");
        String[] uuid = new String[16];
        for (int i = 0; i < 16; i++) {
            uuid[i] = chars[0 | (int)(Math.random() * chars.length)];
        }
        return StringUtils.join(uuid);
    }

    public static void main(String[] args) {
        System.out.println(IDUtil.id());
    }


}
