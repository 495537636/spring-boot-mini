package com.brightlight.dreamcrafter.common.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * @description: 时间工具类
 * @author: LiMG
 * @create: 2024-04-22 14:19:36
 **/
public class TimeUtil {

    public enum Format {
        YMDHMS("yyyy-MM-dd HH:mm:ss"), YMDHMS_CN("yyyy年MM月dd日 HH点mm分ss秒"), MDHM("MM-dd HH:mm"), YMD("yyyy-MM-dd"), HMS(
                "HH:mm:ss"), YMDHMS_FILE("yyyy.MM.dd HH.mm.ss"), compact("yyyyMMddHHmmss");
        private String value;

        Format(String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return this.value;
        }
    }

    /**
     * 指定日期转long
     *
     * @param time
     * @return
     * @throws ParseException
     */
    public static long str2Time(String time, Format _format) throws Exception {// 单位毫秒
        SimpleDateFormat format = new SimpleDateFormat(_format.toString());
        try {
            return format.parse(time).getTime();
        } catch (ParseException e) {
            throw new Exception("parameters '" + e.getMessage() + "' format is wrong");
        }
    }

    /**
     * 获取当前毫秒级时间
     */
    public static long getTime() {// 单位毫秒
        return System.currentTimeMillis();
    }

    /**
     * 获取当前不同格式的日期
     *
     * @param format
     * @return
     */
    public static String getTime(Format format) {
        SimpleDateFormat Format = new SimpleDateFormat(format.toString());
        return Format.format(new Date());
    }

    public static String convertTime(long time, Format format) {
        SimpleDateFormat sdf = new SimpleDateFormat("", Locale.CHINESE);
        sdf.applyPattern(format.toString());
        return sdf.format(time);
    }

    public static Date toTime(String time, Format format) {
        SimpleDateFormat sdf = new SimpleDateFormat("", Locale.CHINESE);
        sdf.applyPattern(format.toString());
        try {
            return sdf.parse(time);
        } catch (ParseException e) {
            return null;
        }
    }

    /**
     * 秒转换毫秒
     *
     * @return
     */
    public static long SEC_MSEL(Object sec) {
        Float sec_f = 0f;
        try {
            sec_f = Float.parseFloat(sec.toString());
        } catch (Exception e) {
        }
        return (long) (sec_f * 1000);
    }

    /**
     * 分转换毫秒
     *
     * @param min
     * @return
     */
    public static long MIN_MSEL(Object min) {
        Float min_f = 0f;
        try {
            min_f = Float.parseFloat(min.toString()) * 60;
        } catch (Exception e) {
        }
        return SEC_MSEL(min_f);
    }

    /**
     * 小时换毫秒
     *
     * @param hr
     * @return
     */
    public static long HR_MSEL(Object hr) {
        Float hr_f = 0f;
        try {
            hr_f = Float.parseFloat(hr.toString()) * 60;
        } catch (Exception e) {
        }
        return MIN_MSEL(hr_f);
    }

    /**
     * 天转换毫秒
     *
     * @param day
     * @return
     */
    public static long DAY_MSEL(Object day) {
        Float day_f = 0f;
        try {
            day_f = Float.parseFloat(day.toString()) * 24;
        } catch (Exception e) {
        }
        return HR_MSEL(day_f);
    }

    public static String minDate(String date1, String date2) {
        try {
            Long time1 = TimeUtil.str2Time(date1, Format.YMD);
            Long time2 = TimeUtil.str2Time(date2, Format.YMD);

            if (time1 > time2) {
                return date2;
            } else {
                return date1;
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static String minDate(String date1, String date2, Format format) {
        try {
            Long time1 = TimeUtil.str2Time(date1, format);
            Long time2 = TimeUtil.str2Time(date2, format);

            if (time1 > time2) {
                return date2;
            } else {
                return date1;
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static String today() {
        return TimeUtil.getTime(TimeUtil.Format.YMD);
    }

    public static String time() {
        return TimeUtil.getTime(TimeUtil.Format.YMDHMS);
    }

}
