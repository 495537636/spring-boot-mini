package com.brightlight.dreamcrafter.common.wrap;

/**
 * @author 张攀钦
 * @date 2020-04-01-14:32
 * 统一返回数据工具类调用方法,赋值 code ,message
 */
public interface IResCodeAndMessage {

    /**
     * 接口状态码
     *
     * @return java.lang.String
     * @author 张攀钦
     * @title code
     */

    Integer resCode();

    /**
     * 返回状态码提示信息
     *
     * @return java.lang.String
     * @author 张攀钦
     * @title message
     */

    String resMessage();
}
