package com.brightlight.dreamcrafter.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.brightlight.dreamcrafter.common.entity.BaseEntity;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import jakarta.persistence.*;
import lombok.Data;

/**
 * @description: 上传文件实体类
 * @author: LiMG
 * @create: 2024-04-22 14:12:48
 **/
@Data
@Entity
@Table(name = "upload_file_info")
@TableName("upload_file_info")
public class UploadFileEntity extends BaseEntity {

    @Id
    @TableId
    @Column(name = "id", columnDefinition = "varchar(255) COMMENT '主键id'")
    @JsonSerialize(using = ToStringSerializer.class)
    private String id;

    /**
     * 文件名称
     */
    @Column(name = "file_name", columnDefinition = "varchar(100) COMMENT '文件名称'")
    private String fileName;

    /**
     * 文件MD5值
     */
    @Column(name = "md5", columnDefinition = "varchar(50) COMMENT '文件MD5值'")
    private String md5;

    /**
     * 当前文件分片索引
     */
    @Column(name = "chunk_index", columnDefinition = "bigint default 0 COMMENT '当前文件分片索引'")
    private Integer chunkIndex;

    /**
     * 文件总分片数
     */
    @Column(name = "chunk_size", columnDefinition = "bigint default 1 COMMENT '文件分片总数'")
    private Integer chunkSize;

    /**
     * 文件上传完成标识
     */
    @Column(name = "finish_state", columnDefinition = "tinyint default 0 COMMENT '文件上传完成标识'")
    private Integer finishState;

    /**
     * 文件存储桶对象名
     */
    @Column(name = "bucket_name", columnDefinition = "varchar(50) COMMENT '文件存储桶对象名'")
    private String bucketName;

    /**
     * 文件存储路径
     */
    @Column(name = "save_path", columnDefinition = "varchar(1000) COMMENT '文件存储路径'")
    private String savePath;

    /**
     * 文件预览地址
     */
    @Transient
    @TableField(exist = false)
    private String previewUrl;

}
