package com.brightlight.dreamcrafter.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.brightlight.dreamcrafter.model.entity.UserInfoEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * @description: 用户信息mapper
 * @author: LiMG
 * @create: 2024-02-04 23:32:00
 **/
@Mapper
public interface UserInfoMapper extends BaseMapper<UserInfoEntity> {
}
