package com.brightlight.dreamcrafter.common.enums;

/**
 * @description: 全局枚举类
 * @author: LiMG
 * @create: 2020-04-14 16:05:05
 **/
public enum BaseEnum {

    /**
     * 请求成功
     */

    SUCCESS(200, "操作成功"),
    UNAUTHORIZED(401, "没有授权"),
    FORBIDDEN(403, "没有权限"),
    ERROR(500, "操作失败"),
    BUSINESS_ERROR(800, "业务异常"),
    NO_LOGIN(10000, "用户未登录或登录信息超时"),
    PARAM_ERROR(701, "参数错误"),
    METHOD_ERROR(702, "请求类型错误"),
    PARAM_MISS(703, "参数缺失"),

    NOT_BUNDLE_LIB(6001, "上传的模型包格式不支持"),
    NOT_SUPPORT_EARTH_URL(6002, "保存的地图 url 不正确"),
    NOT_SUPPORT_MAP_ZIP(6003, "mapzip 包不支持"),
    NOT_SUPPORT_MAP_ZIP_NAME(6004, "效果包的名称不能重复"),


    /**
     * 对象数据相关的校验
     */
    MISS_CATEGORY_TYPE(6502, "保存的数据类型不存在"),
    NOT_PARAM_NEED(6500, "保存或者更新的数据字段多余自定义字段"),
    MISS_NEED_PARAM(6501, "必须的字段没有填写"),
    ERROR_EXCEL_DATA(6503, "上传的 Excel 格式不支持"),
    ERROR_CATEGORY_ID(6504, "分类数据 id 对应分类不存在"),
    ERROR_EXCEL_TYPE(6505, "excel 中 sheet 对应的分类错误"),
    ERROR_MUST_FIELD(6506, "excel 缺少必须字段"),
    ERROR_MISS_FIELD(6507, "excel 字段多余定义的字段"),
    ERROR_DATA_TYPE(6508, "EXCEL 中的数据类型和 sheet 不匹配"),
    ERROR_DATA_TYPE_SAVE_UPDATE(6509, "数据类型与 categoryId 不匹配"),
    ERROR_DATA_SITE_ID(6510, "对象数据中的 siteId 和 传入不符合"),
    DATA_EXIST(6511,"对象数据的 id 已经存在"),
    ERROR_NOT_MUST_FIELD(6512, "excel 缺少定义的字段"),


    /**
     * 告警数据相关
     */
    ALARM_JSON_NULL(7000, "推送的 json 为空"),
    ALARM_MISS_MUST_PARAM(7001, "推送的 json 数据缺失必填字段"),
    ALARM_ERROR_DEVICE(7002, "推送的告警数据关联不到三维中的模型"),
    ALARM_HANDLE_ERROR(7003, "处理的告警数据不能存在"),
    ALARM_HANDLE_CONFIG_MISS(7004, "没有配置告警 url 和alarmId 对应字段"),
    ALARM_HANDLE_FAIL(7005, "告警处理失败"),
    ALARM_STATISTICS_PARAM_ERROR(7006, "sceneId 和 siteId 不能为空"),
    ALARM_STATISTICS_TIMESTAMP_ERROR(7007, "开始时间戳和结束时间戳同时不为 null,或者同时为 null"),
    ALARM_STATISTICS_START_BIG_END(7008, "开始时间戳不能大于结束时间戳"),


    /**
     * 告警策略
     */
    ALARM_STRATEGY_ERROR(7009, "告警策略已经定义"),

    /**
     * 告警配置
     */
    ALARM_CONFIG_EXISTS(7010, "告警等级已经存在"),

    /**
     * 分类已存在
     */
    CLASSIFICATION_EXISTS(801,"值已存在");

    ;


    BaseEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    /**
     * 状态码
     */
    private Integer code;

    /**
     * 消息
     */
    private String message;

    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
