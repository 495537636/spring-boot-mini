package com.brightlight.dreamcrafter;

import com.brightlight.dreamcrafter.mapper.UserInfoMapper;
import com.brightlight.dreamcrafter.model.entity.UserInfoEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class DreamCrafterApplicationTests {

    @Autowired
    private UserInfoMapper userInfoMapper;

    @Test
    void contextLoads() {
    }

    @Test
    void insert() {
        UserInfoEntity userInfo = new UserInfoEntity();
        userInfo.setId(1L);
        userInfo.setUsername("张三");
        userInfo.setPassword("admin111");
        userInfoMapper.insert(userInfo);
        System.out.println("用户信息插入成功...");
    }

    @Test
    void delete() {
        userInfoMapper.deleteById(1L);
        System.out.println("用户信息删除成功...");
    }

    @Test
    void modify() {
        UserInfoEntity userInfo = new UserInfoEntity();
        userInfo.setUsername("李四");
        userInfo.setPassword("admin000");
        userInfo.setId(1L);
        userInfoMapper.updateById(userInfo);
        System.out.println("用户信息修改成功...");
    }

    @Test
    void query() {
        List<UserInfoEntity> userList = userInfoMapper.selectList(null);
        userList.forEach(user -> System.out.println("用户Id=" + user.getId() + ";用户名=" + user.getUsername() + ";密码=" + user.getPassword()));
    }

}
